<?php

/***************************************
 * ACF/GravityForms Stuff
 * *************************************/

 /**
  * Attach images uploaded through Gravity Form to ACF Gallery Field
  *
  * @author Originally by Joshua David Nelson, josh@joshuadnelson.com
  * @author Modified by David Zimmerman for Land Trust Alliance website!
  * @return void
  */

add_filter( "gform_after_submission_5", 'jdn_set_post_acf_gallery_field', 10, 2 );
function jdn_set_post_acf_gallery_field( $entry, $form ) {
	
	$gf_images_field_ids = array(16,17); // the upload field ids, both of them!
	$acf_field_id = 'pledge_photo_gallery'; // the acf gallery field id
	
	// get post
	if( isset( $entry['post_id'] ) ) {
		$post = get_post( $entry['post_id'] );
		if( is_null( $post ) )
			return;
	} else {
		return;
	}
	
  //update the zipcode field in here too
  if ( get_field('zip_code', $post->ID) != '' ) : 
    $ziptastic = file_get_contents('http://ziptasticapi.com/' . get_field('zip_code', $post->ID));
    $zip_info = json_decode($ziptastic);
    if(isset($zip_info->city)) { 
      $address = ucwords(strtolower($zip_info->city)) . ', ' . $zip_info->state;
      update_field('city_state', $address, $post->ID);
    }
  endif;

  // Clean up images upload and create array for gallery field
 	$gallery = array();
  foreach($gf_images_field_ids as $gf_images_field_id) {
    if( isset( $entry[ $gf_images_field_id ] ) && $entry[ $gf_images_field_id ] != '' ) {

      $image = stripslashes( $entry[ $gf_images_field_id ] );
      $image_id = jdn_create_image_id( $image, $post->ID );

  		if( $image_id ) {
  		  $gallery[] = $image_id;
  		}

    }
  }
	
	// Update gallery field with array
	if( ! empty( $gallery ) ) {
		update_field( $acf_field_id, $gallery, $post->ID );
		
		// Updating post
		wp_update_post( $post );
	}

}

/**
 * Create the image attachment and return the new media upload id.
 *
 * @since 1.0.0
 * @see http://codex.wordpress.org/Function_Reference/wp_insert_attachment#Example
 */
function jdn_create_image_id( $image_url, $parent_post_id = null ) {
	
	if( !isset( $image_url ) )
		return false;
	
	// Cache info on the wp uploads dir
	$wp_upload_dir = wp_upload_dir();
	// get the file path
	$path = parse_url( $image_url, PHP_URL_PATH );
	
	// File base name
	$file_base_name = basename( $image_url );
	
  // Full path
  if ( !function_exists( 'get_home_path' ) ) {
  	require_once( dirname(__FILE__) . '/../../../wp-admin/includes/file.php' );
  }
	$home_path = get_home_path();
	$home_path = untrailingslashit( $home_path );
	$uploaded_file_path = $home_path . $path;
	
	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( $file_base_name, null );
	
	// error check
	if( !empty( $filetype ) && is_array( $filetype ) ) {
		
		// Create attachment title
		$post_title = preg_replace( '/\.[^.]+$/', '', $file_base_name );
		
		// Prepare an array of post data for the attachment.
		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $uploaded_file_path ), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => esc_attr( $post_title ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);
		
		// Set the post parent id if there is one
		if( !is_null( $parent_post_id ) )
			$attachment['post_parent'] = $parent_post_id;
			
		// Insert the attachment.
		$attach_id = wp_insert_attachment( $attachment, $uploaded_file_path );
		//Error check
		if( !is_wp_error( $attach_id ) ) {
			//Generate wp attachment meta data
			if( file_exists( ABSPATH . 'wp-admin/includes/image.php') && file_exists( ABSPATH . 'wp-admin/includes/media.php') ) {
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
				$attach_data = wp_generate_attachment_metadata( $attach_id, $uploaded_file_path );
				wp_update_attachment_metadata( $attach_id, $attach_data );
			} // end if file exists check
		} // end if error check

		return $attach_id; 
	
	} // end if $$filetype
} // end function get_image_id

/******************************************************************/
/* Setup custom navigation menus */
/******************************************************************/

function register_base_nav_menus() {

  register_nav_menu('primary', 'Primary Navigation' );
  register_nav_menu('footer', 'Footer Menu');
  register_nav_menu('mobile', 'Mobile Menu' );

}
add_action( 'init', 'register_base_nav_menus' );

/******************************************************************/
/* Setup widget areas */
/******************************************************************/

function setup_widget_areas() {

	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar_1',
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'Footer',
		'id' => 'footer_1',
		'before_widget' => '<div class="footer-widget widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	) );

}
add_action( 'widgets_init', 'setup_widget_areas' );

/******************************************************************
/* Post thumbnails & automatic image sizes *
/******************************************************************/

add_theme_support('post-thumbnails');

add_image_size('hero-small', 480, 9999, false);
add_image_size('hero-medium', 800, 9999, false);
add_image_size('hero-large', 1200, 9999, false);
add_image_size('pledge_thumb', 130, 130, true);
add_image_size('pledge_crop', 700, 700, array('center','top'));

/******************************************************************
/* Excerpt length and readmores *
/******************************************************************/

function custom_excerpt_length( $length ) {
  global $post;
  if(1 == 1) { return 30; }
  else { return $length; }
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
  global $post;
  return '...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );



/******************************************************************/
/* Load the theme's javascript and CSS */
/******************************************************************/

function put_scripts_in_header() {
	wp_enqueue_script('jquery');
	wp_enqueue_style( 'basestyle', get_template_directory_uri() . '/style.php?p=base.scss' );
	wp_enqueue_script('magnific', get_template_directory_uri() . '/js/magnific.js');  
	wp_enqueue_script('svg4everybody', get_template_directory_uri() . '/js/svg4everybody.js');  
  wp_enqueue_script('script', get_template_directory_uri() . '/js/main.js');  
}

add_action('wp_enqueue_scripts', 'put_scripts_in_header');

/******************************************************************
/* Add post types, taxonomies, & miscellaneous features *
/******************************************************************/

function make_post_types_wheeee() {

    $args = array(
      'public' => true,
      'has_archive' => false,
      'label'  => 'Pledges',
      'supports' => array('title','editor','thumbnail','revisions','author','custom-fields')
    );
    register_post_type( 'Pledges', $args );

   $args = array(
      'public' => true,
      'has_archive' => false,
      'label'  => 'Stories',
      'supports' => array('title','editor','thumbnail','revisions','author','custom-fields')
    );
    register_post_type( 'story', $args );

}

add_action( 'init', 'make_post_types_wheeee' );

/******************************************************************
/* Remove junk from head *
/******************************************************************/

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
add_filter( 'index_rel_link', 'disable_stuff' );
add_filter( 'parent_post_rel_link', 'disable_stuff' );
add_filter( 'start_post_rel_link', 'disable_stuff' );
add_filter( 'previous_post_rel_link', 'disable_stuff' );
add_filter( 'next_post_rel_link', 'disable_stuff' );

function disable_stuff( $data ) {
	return false;
}

/******************************************************************/
/* Insert Open Graph tags, use featured image of current post for og:image */
/******************************************************************/

function fb_og_metatags() {
 ?>
    <meta property="og:title" content="<?php the_title(); ?>"/>
    <meta property="og:description" content="<?php echo strip_tags(get_the_excerpt($post->ID)); ?>" />
    <meta property="og:url" content="<?php the_permalink(); ?>"/>
    <?php $fi = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'full'); ?>
    <?php if ($fi) : ?>
    <meta property="og:image" content="<?php echo $fi[0]; ?>"/>
    <?php else: ?>
    <meta property="og:image" content="http://conservationpledge.org/wp-content/uploads/2015/03/cover-home.jpg"/>
    <?php endif; ?>
    <meta property="og:site_name" content="<?php bloginfo('name'); ?>"/>
  <?php
}
add_action('wp_head', 'fb_og_metatags');

/******************************************************************/
/* Extra Stuff */
/******************************************************************/

/* Remove <p> around <img> in content */
function filter_ptags_on_images($content){
  if(get_post_type()=='story') {
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
  }
  else { return $content; }
}
add_filter('the_content', 'filter_ptags_on_images');

/* Remove admin bar in the pledge modals 'cause it's super annoying */
add_action( 'after_setup_theme', 'get_out_admin_bar' );
function get_out_admin_bar() {
  if(isset($_GET['framed'])) {
   show_admin_bar( false );
 }
}

/* Filter previous post/next post links for framed links */
add_filter('next_post_link', 'framed_next_post_link_attributes');
add_filter('previous_post_link', 'framed_prev_post_link_attributes');
 
function framed_next_post_link_attributes($output) {
  if(isset($_GET['framed'])) {
    return str_replace('" rel="next"', '?framed" rel="next" class="prevnext-pledge-btn"', $output);
  } else { 
    return $output;
  }
}
function framed_prev_post_link_attributes($output) {
  if(isset($_GET['framed'])) {
    return str_replace('" rel="prev"', '?framed" rel="prev" class="prevnext-pledge-btn"', $output);
  } else { 
    return $output;
  }
}


/* Stop linking images to themselves when inserted into posts */
update_option('image_default_link_type','none');

/* Remove pagination from pages meta box in custom menu area */

function binda_modify_nav_menu_items_page( $posts, $args, $post_type ){
	global $_nav_menu_placeholder, $nav_menu_selected_id;
 
	$post_type_name = $post_type['args']->name;
 
	$args = array(
		'offset' => $offset,
		'order' => 'ASC',
		'orderby' => 'title',
		'posts_per_page' => -1,
		'post_type' => $post_type_name,
		'suppress_filters' => true,
		'update_post_term_cache' => false,
		'update_post_meta_cache' => false
	);
 
	if ( isset( $post_type['args']->_default_query ) )
		$args = array_merge($args, (array) $post_type['args']->_default_query );
 
	$get_posts = new WP_Query;
	$posts = $get_posts->query( $args );
 
	if ( !$posts )
		$error = '<li id="error">'. $post_type['args']->labels->not_found .'</li>';
 
	// if we're dealing with pages, let's put a checkbox for the front page at the top of the list
	if ( 'page' == $post_type_name ) {
		$front_page = 'page' == get_option('show_on_front') ? (int) get_option( 'page_on_front' ) : 0;
		if ( ! empty( $front_page ) ) {
			$front_page_obj = get_post( $front_page );
			$front_page_obj->front_or_home = true;
			array_unshift( $posts, $front_page_obj );
		} else {
			$_nav_menu_placeholder = ( 0 > $_nav_menu_placeholder ) ? intval($_nav_menu_placeholder) - 1 : -1;
			array_unshift( $posts, (object) array(
				'front_or_home' => true,
				'ID' => 0,
				'object_id' => $_nav_menu_placeholder,
				'post_content' => '',
				'post_excerpt' => '',
				'post_parent' => '',
				'post_title' => _x('Home', 'nav menu home label'),
				'post_type' => 'nav_menu_item',
				'type' => 'custom',
				'url' => home_url('/'),
			) );
		}
	}
 
	return $posts;
}
 
add_action ( 'nav_menu_items_page', 'binda_modify_nav_menu_items_page', 1, 3 );
 
add_action( 'admin_print_styles-' . 'nav-menus.php', 'binda_nav_menu_custom_css' ); 
 
function binda_nav_menu_custom_css() {
	echo '<style type="text/css">#posttype-page .add-menu-item-pagelinks{display:none;}</style>';
}

/* Get current page's depth
 * 0 for top level, 1 for children, 2 for grandchildren, etc
 * http://www.web-templates.nu/2008/09/07/get-depth-like-is_child-is_grandchild/ */
function wt_get_depth($id = '', $depth = '', $i = 0) {
	global $wpdb;
	global $post;

	if($depth == '') {
		if(is_page()) {
			if($id == '') {
				$id = $post->ID;
			}
			$depth = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID = '".$id."'");
			return wt_get_depth($id, $depth, $i);
		}
	}
	elseif($depth == "0") {
		return $i;
	}
	else {
		$depth = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID = '".$depth."'");
		$i++;
		return wt_get_depth($id, $depth, $i);
	}
}

/* Gets top level page's id anywhere in a hierarchy */
function get_post_top_ancestor_id(){
    global $post;
    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }
    return $post->ID;
}

/* 
 * Custom walkers for wp nav menus and wp list pages 
 * Assumes ACF & custom field named "nav_subtitle" is present
 * */

class subtitle_custom_navwalker extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu\">\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';

    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    if(($item->object == 'page') && (get_field('nav_subtitle',$item->object_id)) != '') {
      $item_output .= '<span class="sub-title">'.get_field('nav_subtitle',$item->object_id).'</span>';
    }
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}

}

/* *********************************
 * Custom walker for listing pages 
 * *********************************/

class subtitle_custom_pagelistwalker extends Walker_Page {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class='children'>\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $page, $depth = 0, $args = array(), $current_page = 0 ) {
		if ( $depth )
			$indent = str_repeat("\t", $depth);
		else
			$indent = '';

		extract($args, EXTR_SKIP);
		$css_class = array('page_item', 'page-item-'.$page->ID);

		if( isset( $args['pages_with_children'][ $page->ID ] ) )
			$css_class[] = 'page_item_has_children';

		if ( !empty($current_page) ) {
			$_current_page = get_post( $current_page );
			if ( in_array( $page->ID, $_current_page->ancestors ) )
				$css_class[] = 'current_page_ancestor';
			if ( $page->ID == $current_page )
				$css_class[] = 'current_page_item';
			elseif ( $_current_page && $page->ID == $_current_page->post_parent )
				$css_class[] = 'current_page_parent';
		} elseif ( $page->ID == get_option('page_for_posts') ) {
			$css_class[] = 'current_page_parent';
		}

		$css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );

		if ( '' === $page->post_title )
			$page->post_title = sprintf( __( '#%d (no title)' ), $page->ID );

    $output .= $indent . '<li class="' . $css_class . '"><a href="' . get_permalink($page->ID) . '">' . $link_before . apply_filters( 'the_title', $page->post_title, $page->ID ) . $link_after;
    if((get_field('nav_subtitle',$page->ID)) != '') {
      $output .= '<span class="sub-title">'.get_field('nav_subtitle',$page->ID).'</span>';
    }
    $output .= '</a>';

		if ( !empty($show_date) ) {
			if ( 'modified' == $show_date )
				$time = $page->post_modified;
			else
				$time = $page->post_date;

			$output .= " " . mysql2date($date_format, $time);
		}
	}

	function end_el( &$output, $page, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}

}

/* **********************************************************
 * Support for specific partial subsets of nav menu listings 
 * **********************************************************/

add_filter( 'wp_nav_menu_objects', 'submenu_limit', 10, 2 );

function submenu_limit( $items, $args ) {

    if ( empty($args->submenu) )
        return $items;

    $parent_id = array_pop( wp_filter_object_list( $items, array( 'title' => $args->submenu ), 'and', 'ID' ) );

    //could do it by the page ID too
    //$parent_id = array_pop( wp_filter_object_list( $items, array( 'object_id' => $args->submenu_id ), 'and', 'ID' ) );

    $children  = submenu_get_children_ids( $parent_id, $items );

    foreach ( $items as $key => $item ) {

        if ( ! in_array( $item->ID, $children ) )
            unset($items[$key]);
    }

    return $items;
}

function submenu_get_children_ids( $id, $items ) {

    $ids = wp_filter_object_list( $items, array( 'menu_item_parent' => $id ), 'and', 'ID' );

    foreach ( $ids as $id ) {

        $ids = array_merge( $ids, submenu_get_children_ids( $id, $items ) );
    }

    return $ids;
}

/* **********************************************************
 * Custom walker to list current nav menu item's subnav area
 * **********************************************************/

class thispart_sidebar_walker extends Walker_Nav_Menu {

    var $target_id = false;

    function __construct($target_id = false) {
        $this->target_id = $target_id;
    }

    function walk($items, $depth) {
        $args = array_slice(func_get_args(), 2);
        $args = $args[0];
        $parent_field = $this->db_fields['parent'];
        $target_id = $this->target_id;
        $filtered_items = array();

        // if the parent is not set, set it based on the post
        // or if you're on a blog, use the blog listing page
        // instead of the current post
        // TODO: accommodate taxonomies and other weird pages
        // TODO: probably products too
        if (!$target_id) {
            global $post;
            $postpageid = get_option('page_for_posts');
            foreach ($items as $item) {
                if(is_home() || is_single() || get_post_type() == 'post') {
                    if ($item->object_id == $postpageid) {
                        $target_id = $item->ID;
                    }
                } else {
                    if (($item->object_id == $post->ID) || ($item->object_id == $post->post_parent)) {
                        $target_id = $item->ID;
                    }
                }
            }
        }

        // if there isn't a parent, do a regular menu
        if (!$target_id) return parent::walk($items, $depth, $args);

        // get the top nav item
        $target_id = $this->top_level_id($items, $target_id);

        // only include items under the parent
        // and not the parent itself

        foreach ($items as $item) {
            if (!$item->$parent_field) continue;

            $item_id = $this->top_level_id($items, $item->ID);

            if ($item_id == $target_id) {
                $filtered_items[] = $item;
            }
        }

        return parent::walk($filtered_items, $depth, $args);
    }

    // gets the top level ID for an item ID
    function top_level_id($items, $item_id) {
        $parent_field = $this->db_fields['parent'];

        $parents = array();
        foreach ($items as $item) {
            if ($item->$parent_field) {
                $parents[$item->ID] = $item->$parent_field;
            }
        }

        // find the top level item
        while (array_key_exists($item_id, $parents)) {
            $item_id = $parents[$item_id];
        }

        return $item_id;
    }
}
