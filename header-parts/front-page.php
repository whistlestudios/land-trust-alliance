<section class="hero is-fullscreen" style="background-image: url('http://conservationpledge.org/wp-content/uploads/2015/03/cover-home.jpg');">

  <div class="hero__content">
    <div class="wrap">
      <a class="hero__logo" href="<?php echo home_url(); ?>">
      </a>

      <h1 class="hero__title">Conserving land ensures safe food, clean water,
        fresh air and places to play tomorrow</h1>

      <p class="hero__description">Join our growing list of landowners and take the pledge.</p>

      <ul class="pledges">
        <?php   

          $featured_pledges = get_field('featured_pledges',get_option('page_on_front'));
          if($featured_pledges) :
            foreach($featured_pledges as $post):
              setup_postdata( $post );
         ?>

          <li class="col-1-4">
            <a href="<?php the_permalink(); ?>?framed" class="open-pledge-gallery">
              <figure class="pledge">
                <?php 
                  $images = get_field('pledge_photo_gallery');
                  $zip_code = get_field('zip_code');

                  if(!$images) {
                    $src = "https://maps.googleapis.com/maps/api/staticmap?center=$zip_code&zoom=12&size=130x130&markers=color:red%7C$zip_code";
                  } elseif($images) {
                    $src = $images[0]['sizes']['pledge_thumb']; //first image, pledge_thumb size
                  }
                ?>
                  <img class="pledge__img" src="<?php echo $src; ?>" width="130" height="130" alt="<?php the_title_attribute(); ?> Pledge" />

                <figcaption class="pledge__name">
                  <?php the_title(); ?>
                </figcaption>
              </figure>
            </a>     

          </li><!-- .col-1-4 -->

        <?php endforeach; endif; wp_reset_query(); ?>

        <li class="col-1-4">
          <a href="#content">
            <figure class="pledge">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/crosshair.svg" alt="Make a pledge" class="pledge__img">

              <figcaption class="pledge__name">You</figcaption>
            </figure>          
          </a>
        </li>
      </ul><!-- .pledges -->

    </div><!-- .wrap -->
  </div><!-- .hero-text -->

  <a href="#content" class="arrow">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/arrow.svg" alt="Click to read more" />
  </a>

</section>