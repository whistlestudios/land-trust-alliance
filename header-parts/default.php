<section class="hero" style="background-image: url('http://conservationpledge.org/wp-content/uploads/2015/03/cover-home.jpg')">
	<div class="hero__content">
		<div class="wrap">
			<a class="hero__logo" href="<?php echo home_url(); ?>">
			</a>

			<p class="hero__description">Conserving land ensures safe food, clean water, fresh air and places to play tomorrow.</p>
		</div>
	</div>
</section>