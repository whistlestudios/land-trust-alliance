<?php
  $images = get_field('pledge_photo_gallery');
  $zip_code = get_field('zip_code');
  if($images) { 
    $background = $cropped = $images[0]['sizes']['medium']; 
  }
  else { $background = $cropped = 'https://maps.googleapis.com/maps/api/staticmap?center='. $zip_code . 
    '&zoom=7&size=540x540&format=jpg&markers=color:red%7C'. $zip_code; }
?>
<section class="hero">

  <div class="single-pledge-hero" style="background: url('<?php echo $cropped; ?>') no-repeat center 20%"></div>

<div class="wrap">
  <div class="hero__slider">

    <?php if(!$images): ?>

    <img class="map-mobile map-mobile--single" src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=4&size=640x640&scale=2&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="">

    <div class="map-container">
      <div class="zoom-far">
        <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=4&size=385x680&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="" class="fx">
      </div>
      <div class="zoom-closer">
        <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=5&size=370x350&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="">
      </div>
      <div class="zoom-closest">
        <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=7&size=370x350&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="">
      </div>
    </div><!-- .map-container -->

    <?php elseif($images): ?>

        <?php foreach($images as $image): ?>
          <div class="slide slide--regular" data-cropped="<?php echo $image['sizes']['large']; ?>" style="background: url(<?php echo $image['sizes']['large']; ?>) no-repeat center top">&nbsp;</div>
        <?php endforeach; ?>

        <div class="slide map-container" data-cropped="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=5&size=540x540&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>">

          <img class="map-mobile" src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=4&size=540x540&scale=2&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="">

          <div class="zoom-far">
            <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=4&size=370x680&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="">
          </div>
          <div class="zoom-closer">
            <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=5&size=370x360&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="">
          </div>
          <div class="zoom-closest">
            <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php the_field('zip_code'); ?>&zoom=7&size=370x360&format=jpg&markers=color:red%7C<?php the_field('zip_code'); ?>" alt="">
          </div>
        </div><!-- .map-container -->

    <?php endif; ?>

  </div><!-- .hero__slider -->
</div><!-- .wrap -->

<?php while(have_posts()): the_post(); ?>

<div class="hero__posts-nav">
  
  <?php 

  if(isset($_GET['framed'])) { $framed = '?framed'; } else { $framed = ''; }

  if( get_adjacent_post(false, '', true) ) { 
    previous_post_link('%link', '&larr; Previous Post');
  } else { 
      $first = new WP_Query('posts_per_page=1&order=DESC&post_type=pledges'); $first->the_post();
        echo '<a class="prev" href="' . get_permalink() . $framed . '">&larr; Previous Post</a>';
      wp_reset_query();
  }; 
      
  if( get_adjacent_post(false, '', false) ) { 
    next_post_link('%link', 'Next Post &rarr;');
  } else { 
    $last = new WP_Query('posts_per_page=1&order=ASC&post_type=pledges'); $last->the_post();
        echo '<a class="next" href="' . get_permalink() . $framed . '">Next Post &rarr;</a>';
      wp_reset_query();
  }

  ?>

</div><!-- .hero--posts-nav -->

<?php endwhile; wp_reset_query(); ?>

</section>