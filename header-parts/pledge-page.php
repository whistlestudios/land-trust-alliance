<section class="hero hero--gradient is-fullscreen hero-map">
  <div id="hero-map-canvas"></div>
	<div class="hero__content hero__content--dark">
	  	<div class="wrap">
            <a class="hero__logo" href="<?php echo home_url(); ?>">
            </a>

		    <p class="hero__description hide-mobile">Join our growing list of landowners and take the pledge.</p>
		</div>
	</div>
    <a href="#content" class="arrow">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/arrow.svg" alt="Click to read more" />
    </a>
</section>

<script>
jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var geocoder;
    var current_zoom = (jQuery(window).width() > 700 ? 4 : 3);
    var mapOptions = {
        mapTypeId: 'roadmap',
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: false,
        disableDefaultUI: true,
        backgroundColor: '#b3d4fc',
        zoom: current_zoom,
        center: new google.maps.LatLng(39.5, -98.35)
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("hero-map-canvas"), mapOptions);
    geocoder = new google.maps.Geocoder();    

    // Multiple Markers
    var markers = [<?php

      $args = array(
        'post_type'       => 'Pledges',
        'posts_per_page'  => 8 
      );
      $q = new WP_Query( $args );
      while ( $q->have_posts() ) : $q->the_post();            
        $zips[] = "'".get_field('zip_code')."'";
      endwhile;
      echo implode(',', $zips);

    ?>];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        geocoder.geocode( { 'address': markers[i]}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
          }
        });
    }

    google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        var zoom = (jQuery(window).width() > 700 ? 4 : 3);
        google.maps.event.trigger(map, "resize");
        map.setCenter(center); 
        map.setZoom(zoom);
    });
    
}
</script>