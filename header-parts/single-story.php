<style><?php

  $id = get_the_ID();
  $sizes = array('hero-small','hero-medium','hero-large','full');
  $i = 0;

  foreach($sizes as $size){
    $img = wp_get_attachment_image_src(get_post_thumbnail_id(), $size); 
    if($i == 0) {
      echo ".hero-{$id} { background-image: url({$img[0]}); }";
    } else {
      echo "@media screen and (min-width: {$img[1]}px) { 
        .hero-{$id} { background-image: url({$img[0]}); }
      }";
    }
    $i++;
  }

?></style>

<section class="hero is-fullscreen hero-<?php echo $id; ?>">
  
  <div class="hero__content hero__content--centered">
    <div class="wrap">
      <a class="hero__logo" href="<?php echo home_url(); ?>">
      </a>
        
      <h1 class="hero__title hero__title--big"><?php the_title(); ?></h1>
    </div>
  </div>
  <a href="#content" class="arrow">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/arrow.svg" alt="Click to read more" />
  </a>
</section>