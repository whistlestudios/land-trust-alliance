<?php get_header(); ?>

    <main id="content" class="main-content">
      
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <div class="wrap wrap--narrow">

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <h2 class="entry-title"><?php the_title(); ?></h2>

          <?php the_content(); ?>
            
        </article><!-- #post-<?php the_ID(); ?> -->

      </div>

    <?php endwhile; endif; ?>

  </main><!-- .main-content -->

<?php get_footer(); ?>
