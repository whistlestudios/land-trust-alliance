<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf8">
  <?php if(isset($_GET['forcefull'])) { ?><meta name="viewport" content="width=1024, initial-scale=1">
  <?php } else { ?><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><?php } ?>
  <title><?php wp_title(''); ?></title>

  <?php wp_head(); ?> 
  
  <?php if(!isset($_GET['framed'])): ?>
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55158bda4ab4b24f" async="async"></script>
  <?php endif; ?>

</head>

<body <?php if(isset($_GET['framed'])) { body_class('is_framed'); } else { body_class(); } ?> >
  <div id="page">
    <header class="page-header" role="banner">

      <?php if(!isset($_GET['framed'])): ?>
      
      <a href="http://www.landtrustalliance.org/" target="_blank">

        <svg class="logo">
          <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/sprite.svg#logo" /></use>
        </svg>    

      </a>

      <div class="hamburger">
        <a href="">
          <span class="line-one"></span>
          <span class="line-two"></span>
          <span class="line-three"></span>
        </a>

        <span class="hamburger__title">Menu</span>
      </div>

      <?php endif; ?>

    </header>

    <nav class="nav-primary" role="navigation">
      <?php  wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
    </nav>

    <?php 
    if( is_front_page() ): 
      include('header-parts/front-page.php');

    elseif( is_page('pledges') ): 
      include('header-parts/pledge-page.php');

    elseif( is_singular('pledges') ): 
      include('header-parts/single-pledge.php');

    elseif( is_singular('story') ):
      include('header-parts/single-story.php');

    else :
      include('header-parts/default.php');

    endif;
    ?>