<?php get_header(); ?>

<main id="content" class="main-content">

  <?php while(have_posts()): the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class('story'); ?>>
      <div class="wrap wrap--narrow">
          <h2><?php the_title(); ?></h2>

          <?php if ( get_field('zip_code') != '' ) : ?> 
            <span class="location" data-zip="<?php the_field('zip_code'); ?>"><?php the_field('city_state'); ?></span>
          <?php endif; ?>

          <?php the_content(); ?>

          <?php if(get_field('meaningful_story')): ?>
            <div class="meaningful_story">
              <?php the_field('meaningful_story'); ?>
            </div>
          <?php endif; ?>

          <span class="date"><strong>Pledged:</strong> <?php echo get_the_date(); ?></span>

          <span class="copyright">Map data &copy;<?php the_date('Y'); ?> Google</span>
      </div>
    </article>

  <?php endwhile; ?>

</main>

<?php if(!isset($_GET['framed'])): ?>

<?php get_footer(); ?>

<?php endif; ?>