<?php 

/*

Template Name: Pledges Listing Page 

*/

get_header(); 

?>

    <main id="content" class="main-content"> 
      <div class="wrap">
        <h2 class="hide-desktop title-centered">Join our growing list of landowners and take the pledge.</h2>
      <ul class="pledges">
        <?php   

          $args = array(
            'post_type'       => 'Pledges',
            'posts_per_page'  => 12           
          );
        
          $query = new WP_Query( $args );

         ?>

        <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();  ?>

          <li class="col-1-4">
            <a href="<?php the_permalink(); ?>?framed" class="open-pledge-gallery">
              <figure class="pledge">
                <?php 
                  $images = get_field('pledge_photo_gallery');
                  $zip_code = get_field('zip_code');

                  if(!$images) {
                    $src = "https://maps.googleapis.com/maps/api/staticmap?center=$zip_code&zoom=12&size=130x130&markers=color:red%7C$zip_code";
                  } elseif($images) {
                    $src = $images[0]['sizes']['pledge_thumb']; //first image, pledge_thumb size
                  }
                ?>
                  <img class="pledge__img" src="<?php echo $src; ?>" width="130" height="130" alt="<?php the_title_attribute(); ?> Pledge" />

                <figcaption class="pledge__name pledge__name--colored">
                  <?php the_title(); ?>

                  <?php if ( get_field('zip_code') != '' ) : ?> 
                    <span class="location" data-zip="<?php the_field('zip_code'); ?>"><?php the_field('city_state'); ?></span>
                  <?php endif; ?>

                </figcaption>
              </figure>
            </a>     

          </li><!-- .col-1-4 -->

        <?php endwhile; endif; ?>

      </ul><!-- .pledges -->
    </div><!-- .wrap -->

      <div class="call-to-action">
        <div class="wrap wrap--narrow">
          <h2>Will you take the pledge today?</h2>

          <p>The pledge is open to individuals and families whose property is more than 50 acres. Initially however, many of the pledges will be from many of those who are the largest landowners in the country. By encouraging the largest private land-holdings in America, to take the pledge, it will serve to raise the profile of private land conservation and inspire countless others to conserve their own lands.</p>
          
          <p>By taking the pledge, it is a moral commitment to conserve ones’ land, not a legal contract. Since transactions to conserve private lands can take many forms, and the timing of those transactions will depend on different family, financial and other factors, few families can commit right now to exactly how and when their particular land(s) will be conserved. This Conservation Pledge allows families and individuals to express publicly their commitment to assure that at some time during their lives, or pursuant to their wills, at least half of their land will be conserved.</p>

          <a href="#form-modal" class="btn open-popup-link">Pledge Now</a>    

          <a class="addthis_button_compact share"><span>Share This</span></a> 
        </div><!-- .wrap -->
      </div><!-- .call-to-action -->
  </main><!-- .main-content -->

<?php get_footer(); ?>

