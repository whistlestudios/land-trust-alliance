</div><!-- #page -->

<div id="form-modal" class="mfp-hide modal">
  <div id="form">
    <?php echo do_shortcode( '[gravityform id="5" title="true" description="false" ajax="true"]' ) ?>
  </div>
</div>

<?php if(!is_404()): ?>
<div class="pre-footer">
  <div class="wrap wrap--narrow">
    <p>Founded in 1982, the Land Trust Alliance is a national land conservation organization that works to save the places people love by strengthening land conservation across America. The Alliance represents 1,100 member land trusts supported by more than 100,000 volunteers and 5 million members nationwide. The Alliance is based in Washington, D.C. and operates several regional offices.</p>
  </div><!-- .wrap -->
</div><!-- .about --> 
<?php endif; ?>

<footer class="footer" role="contentinfo">
  <div class="wrap">
    <div class="col-2-3">
      <h3>Recent Updates</h3>

      <ul class="footer__news">

        <?php 

          $args = array(
            'posts_per_page'   => 3,
            'post_type'        => 'post'
          );

          $posts = get_posts( $args );

        ?>

        <?php foreach ($posts as $post) : setup_postdata($post); ?>

          <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </li>
      
        <?php 

          wp_reset_postdata();

          endforeach; 

        ?>

      </ul>
    </div>

    <div class="col-1-3">
      <svg class="footer__logo">
        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/img/sprite.svg#logo-footer" /></use>
      </svg>          
      
      <div class="addthis_vertical_follow_toolbox clear"></div>
    </div>

    <div class="footer__nav clear full-width">
      <?php wp_nav_menu( array('theme_location' => 'footer') ); ?>

      <span class="copyright">&copy; Copyright <?php the_date('Y'); ?> Land Trust Alliance</span>  
    </div>
  </div><!-- .wrap -->
</footer><!-- .colophon -->

<?php wp_footer(); ?>
</body>
</html>
