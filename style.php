<?php

require "scss/scss.inc.php";

$scss = new scssc();
$scss->setFormatter("scss_formatter_compressed");
$scss->setImportPaths("scss");

$server = new scss_server("scss", null, $scss);
$server->serve();

