<?php get_header(); ?>

  <main id="content" class="main-content" role="main">

    <div class="wrap wrap--narrow">
      
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article <?php post_class(); ?>>

          <?php the_content(); ?>
            
        </article><!-- #post-<?php the_ID(); ?> -->

      <?php endwhile; endif; ?>    

    </div><!-- .wrap -->

    <div class="call-to-action">
      <div class="wrap">
        <a href="#form-modal" class="btn open-popup-link">Pledge Now</a>    

        <a class="addthis_button_compact share"><span>Share This</span></a> 
      </div><!-- .wrap -->
    </div><!-- .call-to-action -->

    <?php   
      $query = new WP_Query(array('post_type' => 'story', 'posts_per_page'  => -1));
      if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();  
        while(have_rows('teaser')): the_row();
      ?>
          <div class="backdrop backdrop--quote">
              <div class="background" style="background-image: url('<?php
                $img = wp_get_attachment_image_src(get_sub_field('blurry_background_image', get_the_ID()), 'medium');
                echo $img[0];
              ?>');"></div>

              <blockquote>
                <?php the_sub_field('quotation'); ?>

                <cite>- <?php the_sub_field('name'); ?></cite>
              </blockquote>

              <a href="<?php the_permalink(); ?>" class="btn btn--no-bg">Read More</a>
          </div>
    <?php endwhile; 
          endwhile; endif; wp_reset_query(); ?>

    <div class="wrap">
      <section class="pledges-wrap">    
        <div class="wrap wrap--medium">
          <h2><a href="<?php echo home_url(); ?>/pledges">Recent Pledges</a></h2>

          <p>Join a growing list of people who have pledged to conserve privately owned land for future generations to come.</p>
        </div>

      <ul class="pledges">
        <?php   

          $args = array(
            'post_type'       => 'Pledges',
            'posts_per_page'  => 8 
          );
        
          $query = new WP_Query( $args );

         ?>

        <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();  ?>

          <li class="col-1-4">
            <a href="<?php the_permalink(); ?>?framed" class="open-pledge-gallery">
              <figure class="pledge">
                <?php 
                  $images = get_field('pledge_photo_gallery');
                  $zip_code = get_field('zip_code');

                  if(!$images) {
                    $src = "https://maps.googleapis.com/maps/api/staticmap?center=$zip_code&zoom=12&size=130x130&markers=color:red%7C$zip_code";
                  } elseif($images) {
                    $src = $images[0]['sizes']['pledge_thumb']; //first image, pledge_thumb size
                  }
                ?>
                  <img class="pledge__img" src="<?php echo $src; ?>" width="130" height="130" alt="<?php the_title_attribute(); ?> Pledge" />

                <figcaption class="pledge__name pledge__name--colored">
                  <?php the_title(); ?>

                  <?php if ( get_field('zip_code') != '' ) : ?> 
                    <span class="location" data-zip="<?php the_field('zip_code'); ?>"><?php the_field('city_state'); ?></span>
                  <?php endif; ?>

                </figcaption>
              </figure>
            </a>     

          </li><!-- .col-1-4 -->

        <?php endwhile; endif; ?>

      </ul><!-- .pledges -->

      </section><!-- .pledge-wrap -->
    </div><!-- .wrap -->
  </main><!-- .main-content -->

<?php get_footer(); ?>

