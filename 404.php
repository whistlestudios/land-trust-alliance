<?php 

/* 

Template Name: 404

*/

get_header(); 

?>

    <main id="content" class="main-content">

      <div class="wrap wrap--narrow">

        <article class="four-oh-four">
          <h1 class="entry-title">Not Found</h1>

          Sorry, this content could not be found.
            
        </article><!-- #post-<?php the_ID(); ?> -->

      </div>

  </main><!-- .main-content -->

<?php get_footer(); ?>
