<?php get_header(); ?>

<div class="wrap">
  <div class="col-2-3 right">
	  <section class="main-content" role="main">
    
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <h1 class="entry-title">
            <?php if(!is_single()) : ?><a href="<?php the_permalink(); ?>"><?php endif; 
               the_title(); 
                  if(!is_single()) : ?></a><?php endif; ?>
          </h1>

					<?php if(is_single()) { the_content(); } else { the_excerpt(); } ?>

        </article><!-- #post-<?php the_ID(); ?> -->

      <?php endwhile; ?>

      <?php else : ?>

        <h1>Not Found</h1>
        <p>Nothing to display.</p>

      <?php endif; ?>

	  </section><!-- .main-content -->
  </div><!-- .col-2-3 -->

  <?php get_sidebar(); ?>

</div><!-- .wrap -->

<?php get_footer(); ?>
