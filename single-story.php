<?php 

get_header(); 

?>

    <main id="content" class="main-content">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class('story'); ?>>
        <?php the_content(); ?>
      </article><!-- #post-<?php the_ID(); ?> -->

        <div class="call-to-action">
          <div class="wrap">
            <a href="#form-modal" class="btn open-popup-link">Pledge Now</a>    

            <a class="addthis_button_compact share"><span>Share This</span></a> 
          </div><!-- .wrap -->
        </div><!-- .call-to-action -->

    <?php endwhile; endif; ?>

  </main><!-- .main-content -->

<?php get_footer(); ?>
