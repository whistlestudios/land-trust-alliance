jQuery(document).ready(function($){

  $('#jump-top').on('click',function(){
    $('.wrap-mobile-nav ul').toggleClass('active');
  });

  //open/close primary navigation
  $('.hamburger:not(.mfp-burger)').on('click', function(e){
    e.preventDefault();
    $('.hamburger a').toggleClass('is-active');

    if($('.hamburger a').hasClass('is-active')) {
      $('.hamburger__title').text('CLOSE');
    }
    else { $('.hamburger__title').text('MENU'); }

    $('.page-header').toggleClass('menu-is-open');
    
    //in firefox transitions break when parent overflow is changed, so we need to wait for the end of the transition to give the body an overflow hidden
    if( $('.nav-primary ul').hasClass('is-visible') ) {
      $('.nav-primary ul').removeClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
        $('body').removeClass('nav-is-open');
      });
    } else {
      $('.nav-primary ul').addClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
        $('body').addClass('nav-is-open');
      }); 
    }
  });

  // To display form modal
  $('.open-popup-link').magnificPopup({
    type:'inline',
    midClick: true,
    focus: '#input_5_8'
  });

  // Defining a function to set size for #hero 
  function fullscreen(){
    $('.is-fullscreen').css({
      width: $(window).width(),
      height: $(window).height()
    });
  }

  fullscreen();

  // Run the function in case of window resize
  $(window).resize(function() {
     fullscreen();         
  });

  // $('.hero__title').fitText(2.5, { minFontSize: '20px', maxFontSize: '42px' });

  $(function() {
    $('a[href=#content]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });

  // pledge modal functions

  $('.open-pledge-gallery').magnificPopup({
    type:'iframe',
    closeBtnInside: true,
    overflowY: 'hidden',
    midClick: true,
    closeMarkup: '<div class="mfp-close"><div class="hamburger mfp-burger"><span class="is-active"><span class="line-one"></span><span class="line-two"></span><span class="line-three"></span></span><span class="hamburger__title">Close</span></div></div>',
    mainClass: 'mfp-slidein'
  });

  // ken burns and hero background

  if($('.hero__slider').length){
    setTimeout(function(){
      $('.hero__slider').each(function(){
        var $slides = $(this).find('.slide');

        $slides.first().addClass('fx');

        setInterval(function(){
          var $current = $slides.filter('.fx');
          var $next = $current.next().length ? $current.next() : $slides.first();
          $current.removeClass('fx');
          $next.addClass('fx');
          /*if($next.attr('data-cropped')) {
            $('.single-pledge-hero').css('backgroundImage', 'url(' + $next.attr('data-cropped') + ')');
          }*/

        }, 4500);
      });
    }, 700);
  }

  //ziptastic
  var zips = [];

  $('span.location').each(function(){
    var zip = $(this).data('zip');
    zips.push(zip);
  });

  $.unique(zips);
  
  $.each(zips, function(i, zip){
    var sel = "span.location[data-zip='"+zip+"']";
    if($(sel).html() == '') {
      $.ajax({
        url: "http://ZiptasticAPI.com/"+zip,
        dataType: 'json',
        success: function(z){
          var r = ucwords(z.city.toLowerCase()) + ', ' + z.state;
          $(sel).html(r);
        }
      });
    }

  })

  //ga tracking

  $('.call-to-action .open-popup-link').on('click',function(){
    //_gaq.push(['_trackEvent', 'Call-to-action', 'Click', 'Pledge Now Button']);
  });

  $('.call-to-action .share').on('click',function(){
    //_gaq.push(['_trackEvent', 'Call-to-action', 'Click', 'Share Button']);
  });

  $('.open-pledge-gallery').on('click',function(){
    var pledgetitle = $(this).find('.pledge__name').contents()
      .filter(function() {
        return this.nodeType === 3;
      }).text();

    pledgetitle = $.trim(pledgetitle);
    
    pledgelink = $(this).attr('href');
    pledgelink = pledgelink.substring(0, pledgelink.indexOf('?')); 
    
    /* while we're here, update the URL in the address bar */
    window.history.pushState('', pledgetitle, pledgelink);

    //_gaq.push(['_trackEvent', 'Pledge Modal', 'Open', pledgetitle]);
  });

  $('.hamburger').on('click', function(){
    //_gaq.push(['_trackEvent', 'Main Menu', 'Click']);

  });

  /* More address bar hacking */

  $('.prevnext-pledge-btn').on('click',function(){
    pledgelink = $(this).attr('href');
    pledgelink = pledgelink.substring(0, pledgelink.indexOf('?')); 
    /* update the parent window */
    window.top.history.pushState('','', pledgelink);
  });

  var original_place = window.location.href;

  $(document).on('click','.mfp-iframe-scaler .mfp-close', function(){
    window.history.pushState('','',original_place);
    $('.mfp-slidein').removeClass('mfp-ready');
    setTimeout(function(){ $.magnificPopup.close(); }, 500);
  });

  /* addthis in gform confirmation message */
  $(document).bind('gform_confirmation_loaded', function(event, formId){
    addthis.layers.refresh();
  });

});

function ucwords(str) {
  //  discuss at: http://phpjs.org/functions/ucwords/
  // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // improved by: Waldo Malqui Silva (http://waldo.malqui.info)
  // improved by: Robin
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  //    input by: James (http://www.james-bell.co.uk/)
  //   example 1: ucwords('kevin van  zonneveld');
  //   returns 1: 'Kevin Van  Zonneveld'
  //   example 2: ucwords('HELLO WORLD');
  //   returns 2: 'HELLO WORLD'

  return (str + '')
    .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
      return $1.toUpperCase();
    });
}